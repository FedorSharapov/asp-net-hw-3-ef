﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class AddDiscountPromocod : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Discount",
                table: "PromoCodes",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "PromoCodes",
                keyColumn: "Id",
                keyValue: new Guid("cb98e283-fcb0-4baa-8541-39ffe9a3fe38"),
                columns: new[] { "BeginDate", "Discount", "EndDate" },
                values: new object[] { new DateTime(2023, 3, 20, 19, 56, 24, 689, DateTimeKind.Local).AddTicks(5369), 20, new DateTime(2023, 4, 3, 19, 56, 24, 694, DateTimeKind.Local).AddTicks(7914) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Discount",
                table: "PromoCodes");

            migrationBuilder.UpdateData(
                table: "PromoCodes",
                keyColumn: "Id",
                keyValue: new Guid("cb98e283-fcb0-4baa-8541-39ffe9a3fe38"),
                columns: new[] { "BeginDate", "EndDate" },
                values: new object[] { new DateTime(2023, 3, 20, 18, 19, 49, 959, DateTimeKind.Local).AddTicks(6876), new DateTime(2023, 4, 3, 18, 19, 49, 963, DateTimeKind.Local).AddTicks(2913) });
        }
    }
}
