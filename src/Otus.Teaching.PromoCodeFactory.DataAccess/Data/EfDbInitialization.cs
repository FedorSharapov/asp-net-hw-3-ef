﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions;
using Otus.Teaching.PromoCodeFactory.DataAccess.Context;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitialization : IDbInitialization
    {
        private readonly DatabaseContext _context;

        public EfDbInitialization(DatabaseContext databaseContext)
        {
            _context =  databaseContext;
        }

        public void Init()
        {
            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();

            _context.Roles.AddRange(FakeDataFactory.Roles);
            _context.Employees.AddRange(FakeDataFactory.Employees);
            _context.Preferences.AddRange(FakeDataFactory.Preferences);
            _context.Customers.AddRange(FakeDataFactory.Customers);
            _context.PromoCodes.AddRange(FakeDataFactory.PromoCodes);
            _context.SaveChanges();
        }
    }
}
