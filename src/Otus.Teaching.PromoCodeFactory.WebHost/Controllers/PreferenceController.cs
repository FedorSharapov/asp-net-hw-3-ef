﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PreferenceController : ControllerBase
    {
        private readonly IRepository<Preference> _preferenceRepository;

        public PreferenceController(IRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить все предпочтения
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PrefernceResponse>>> GetPrefernceAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();

            var preferenceModelList = preferences.Select(p =>
                new PrefernceResponse()
                {
                    Id = p.Id,
                    Name = p.Name
                }).ToList();

            return preferenceModelList;
        }

        /// <summary>
        /// Получить предпочтение по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PrefernceResponse>> GetPrefernceAsync(Guid id)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);

            if (preference == null)
                return NotFound();

            var preferenceModel = new PrefernceResponse()
            {
                Id = preference.Id,
                Name = preference.Name
            };

            return preferenceModel;
        }
    }
}
