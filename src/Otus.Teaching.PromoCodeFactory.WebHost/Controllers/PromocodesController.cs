﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;

        public PromocodesController(IRepository<PromoCode> promoCodeRepository, IRepository<Preference> preferenceRepository, IRepository<Customer> customerRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promoCodeRepository.GetAllAsync();

            var promocodeModelList = promocodes.Select(p =>
                new PromoCodeShortResponse()
                {
                    Id = p.Id,
                    Code = p.Code,
                    ServiceInfo = p.ServiceInfo,
                    Discount = p.Discount,
                    BeginDate = p.BeginDate.ToString("yyyy.MM.dd"),
                    EndDate = p.EndDate.ToString("yyyy.MM.dd"),
                    PartnerName = p.PartnerName
                }).ToList();

            return promocodeModelList;
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preference = await _preferenceRepository
                .FirstOrDefaultAsync(p => p.Name == request.Preference);

            if (preference == null)
                return BadRequest();

            var customer = await _customerRepository
                .FirstOrDefaultAsync(c => c.Preferences
                .Any(cp => cp.PreferenceId == preference.Id));

            if (customer == null)
                return NotFound($"Нет пользователей с предпочтением \"{request.Preference}\"");

            var promocod = new PromoCode()
            {
                ServiceInfo = request.ServiceInfo,
                Discount = request.Discount,
                PartnerName = request.PartnerName,
                Code = request.PromoCode,
                Preference = preference,
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(14),
                Customer = customer
            };

            await _promoCodeRepository.AddAsync(promocod);

            return Ok();
        }
    }
}