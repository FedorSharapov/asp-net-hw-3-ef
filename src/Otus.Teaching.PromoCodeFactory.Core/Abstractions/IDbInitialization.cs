﻿namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions
{
    public interface IDbInitialization
    {
        public void Init();
    }
}
